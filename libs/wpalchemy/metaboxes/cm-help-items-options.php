<div class="my_meta_control cm-help-items-options">

    <?php
    wp_print_styles('editor-buttons');

    ob_start();
    wp_editor('', 'content', array(
        'dfw' => true,
        'editor_height' => 1,
        'tinymce' => array(
            'resize' => true,
            'add_unload_trigger' => false,
        ),
    ));
    $content = ob_get_contents();
    ob_end_clean();

    $args = array(
        'post_type' => 'page',
        'show_option_none' => CMPopUpFlyIn::__('None'),
        'option_none_value' => '',
    );

    add_filter('the_editor_content', 'wp_richedit_pre');
    $switch_class = 'tmce-active';
    
    $defaultWidgetType = CMPOPFLY_Settings::getOption(CMPOPFLY_Settings::OPTION_DEFAULT_WIDGET_TYPE);
    $widgetType = CMPOPFLY_Settings::getOptionConfig(CMPOPFLY_Settings::OPTION_DEFAULT_WIDGET_TYPE);
    $displayMethod = CMPOPFLY_Settings::getOptionConfig(CMPOPFLY_Settings::OPTION_DISPLAY_METHOD);
    $widgetDisplayMethod = CMPOPFLY_Settings::getOptionConfig(CMPOPFLY_Settings::OPTION_DISPLAY_METHOD);
    $widgetShape = CMPOPFLY_Settings::getOptionConfig(CMPOPFLY_Settings::OPTION_CUSTOM_WIDGET_SHAPE);
    $widgetShowEffect = CMPOPFLY_Settings::getOptionConfig(CMPOPFLY_Settings::OPTION_CUSTOM_WIDGET_SHOW_EFFECT);
    $widgetInterval = CMPOPFLY_Settings::getOptionConfig(CMPOPFLY_Settings::OPTION_CUSTOM_WIDGET_INTERVAL);
    $underlayType = CMPOPFLY_Settings::getOptionConfig(CMPOPFLY_Settings::OPTION_CUSTOM_WIDGET_UNDERLAY_TYPE);
    $selectedBanner = CMPOPFLY_Settings::getOptionConfig(CMPOPFLY_Settings::OPTION_CUSTOM_WIDGET_SELECTE_BANNER);
    $activityDates = get_post_meta($_GET['post'], CMPopUpFlyInShared::CMPOPFLY_CUSTOM_ACTIVITY_DATES_META_KEY);
    if(!empty($activityDates)){
        $activityDates = maybe_unserialize($activityDates[0]);
    }else{
        $activityDates = false;
    }
    ?>
    <label>Widget type</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-type'); ?>
        <select name="<?php $mb->the_name(); ?>" id="cm-campaign-widget-type">
            <?php
            $fieldValue = $mb->get_the_value();
//            echo '<option value="0" ' . selected('0', $fieldValue, false) . '>' . CMPopUpFlyIn::__('Default') . ' (' . $widgetType['options'][$defaultWidgetType] . ') </option>';
            foreach ($widgetType['options'] as $key => $value) {
                echo '<option value="' . $key . '" ' . selected($key, $fieldValue, false) . '>' . $value . '</option>';
            }
            ?>
        </select><br />
        <span class='field-info'>You can choose the different Widget Type for the current Campaign.</span>
    </p>
    <label>Display method</label>
    <p>
        <span class="floatLeft">
            <?php $mb->the_field('cm-campaign-display-method');
            $fieldValue = $mb->get_the_value();
            if(empty($fieldValue)){
                $fieldValue = $widgetDisplayMethod['default'];
            }
            foreach ($widgetDisplayMethod['options'] as $key => $value) {
                echo '<input name="' . $mb->get_the_name() . '" type="radio" value="' . $key . '" ' . checked($key, $fieldValue, false) . ' class="campaign-display-method">' . $value . "<br />";
            }
            ?>
        </span>
        <span id='campaign-selected-banner-panel' class="floatLeft" style="display: none;">
            Selected banner:
            <?php $mb->the_field('cm-campaign-widget-selected-banner'); ?>
            <input type="hidden" id="campaign-selected-banner-back" value="<?php echo $fieldValue = $mb->get_the_value() ?>"/>
            <select name="<?php $mb->the_name(); ?>" id="cm-campaign-widget-selected-banner"></select>
        </span>
        <div class="clear"></div>
        <br />
        <span class='field-info'>You can choose the different Widget Display Method for the current Campaign.</span>
    </p>
    <label>Widget width</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-width'); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" placeholder="250px" value="<?php echo $metabox->get_the_value(); ?>"/>
        <span class='field-info'>Campaign widget width. If blank defaults to 250px</span>
    </p>
    <label>Widget height</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-height'); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" placeholder="350px" value="<?php echo $metabox->get_the_value(); ?>"/>
        <span class='field-info'>Campaign widget height. If blank defaults to 350px</span>
    </p>
    <label>Background color</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-background-color'); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" placeholder="#ffffff" value="<?php echo $metabox->get_the_value(); ?>"/>
        <span class='field-info'>Campaign widget background color. Please enter it in hash color format (eg. #abc123). If blank defaults to #ffffff (white)</span>
    </p>
    <label>Delay to show</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-delay-to-show'); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" placeholder="0" value="<?php echo $metabox->get_the_value(); ?>"/>
        <span class='field-info'>Campaign widget time between page loads and appearing of the widget. If blank defaults to 0s.</span>
    </p>
    <label>Widget shape</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-shape'); ?>
        <select name="<?php $mb->the_name(); ?>">
            <?php
            $fieldValue = $mb->get_the_value();
            if(empty($fieldValue)){
                $fieldValue = $widgetShape['default'];
            }
//            echo '<option value="0" ' . selected('0', $fieldValue, false) . '>' . CMPopUpFlyIn::__('Default') . ' (' . $widgetType['options'][$defaultWidgetType] . ') </option>';
            foreach ($widgetShape['options'] as $key => $value) {
                echo '<option value="' . $key . '" ' . selected($key, $fieldValue, false) . '>' . $value . '</option>';
            }
            ?>
        </select>
        <br />
        <span class='field-info'>You can choose the different Widget Shape for the current Campaign.</span>
    </p>
    <label>Widget show effect</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-show-effect'); ?>
        <select name="<?php $mb->the_name(); ?>">
            <?php
            $fieldValue = $mb->get_the_value();
            if(empty($fieldValue)){
                $fieldValue = $widgetShowEffect['default'];
            }
//            echo '<option value="0" ' . selected('0', $fieldValue, false) . '>' . CMPopUpFlyIn::__('Default') . ' (' . $widgetType['options'][$defaultWidgetType] . ') </option>';
            foreach ($widgetShowEffect['options'] as $key => $value) {
                echo '<option value="' . $key . '" ' . selected($key, $fieldValue, false) . '>' . $value . '</option>';
            }
            ?>
        </select>
        <br />
        <span class='field-info'>You can choose the different Widget Show Effect for the current Campaign.</span>
    </p>
    <label>Widget show interval</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-interval'); ?>
        <select name="<?php $mb->the_name(); ?>" id="user_show_method-flying-bottom">
            <?php
            $fieldValue = $mb->get_the_value();
            if(empty($fieldValue)){
                $fieldValue = $widgetInterval['default'];
            }
//            echo '<option value="0" ' . selected('0', $fieldValue, false) . '>' . CMPopUpFlyIn::__('Default') . ' (' . $widgetType['options'][$defaultWidgetType] . ') </option>';
            foreach ($widgetInterval['options'] as $key => $value) {
                echo '<option value="' . $key . '" ' . selected($key, $fieldValue, false) . '>' . $value . '</option>';
            }
            ?>
        </select>
        <br />
        <span class='field-info'>You can choose the different Widget Show Interval for the current Campaign.</span>
    </p>
    <span id="resetFloatingBottomBannerCookieContainer" style="display: none;">
    <label>Interval reset time</label>
        <p>
            <?php $mb->the_field('cm-campaign-widget-interval_reset_time'); ?>
            <input type="text" name="<?php $mb->the_name(); ?>" placeholder="0" value="<?php echo $metabox->get_the_value(); ?>"/>
            <span class='field-info'>After how many days after first impression widget should appear again. If blank defaults to 7 days.</span>
        </p>
    </span>
    <span id="underlayTypeContainer" style="display: none;">
    <label>Underlay type</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-underlay-type'); ?>
        <select name="<?php $mb->the_name(); ?>">
            <?php
            $fieldValue = $mb->get_the_value();
            if(empty($fieldValue)){
                $fieldValue = $underlayType['default'];
            }
            foreach ($underlayType['options'] as $key => $value) {
                echo '<option value="' . $key . '" ' . selected($key, $fieldValue, false) . '>' . $value . '</option>';
            }
            ?>
        </select>
        <br />
        <span class='field-info'>You can choose the different Widget Underlay Type for current Campaign.</span>
    </p>
    </span>
    <label>Activity Dates</label>
    <p>
        <div id="dates">
                <?php
                $mb->the_field('cm-campaign-widget-activity-dates');
                $datesInputName = $mb->get_the_name();
                if(!empty($activityDates)){
                foreach($activityDates AS $date)
                    {
                        echo '<div class="date_range_row">';
                        echo '<input type="text" name="'. $datesInputName .'[date_from][]" class="date" value="' . $date['date_from'] . '" readonly/>&nbsp;';
                        echo '<input class="h_spinner ac_spinner" name="'. $datesInputName .'[hours_from][] " value="' . $date['hours_from'] . '" readonly/>&nbsp;h&nbsp;';
                        echo '<input class="m_spinner ac_spinner" name="'. $datesInputName .'[mins_from][]" value="' . $date['mins_from'] . '" readonly/>&nbsp;m';
                        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="' . CMPOPFLY_PLUGIN_URL . 'shared/assets/images/arrow_right.png' . '" style="vertical-align:bottom" />&nbsp;&nbsp;&nbsp;&nbsp;';
                        echo '<input type="text" name="'. $datesInputName .'[date_till][]" class="date" value="' . $date['date_till'] . '" readonly/>&nbsp;';
                        echo '<input class="h_spinner ac_spinner" name="'. $datesInputName .'[hours_to][]" value="' . $date['hours_to'] . '" readonly/>&nbsp;h&nbsp;';
                        echo '<input class="m_spinner ac_spinner" name="'. $datesInputName .'[mins_to][]" value="' . $date['mins_to'] . '" readonly/>&nbsp;m&nbsp;';
                        echo '<a href="#" class="delete_link"><img src="' . CMPOPFLY_PLUGIN_URL . 'shared/assets/images/close.png' . '" /></a>';
                        echo '</div>';
                    }
                }
                else echo 'There are no limitations set';
                ?>
        </div>
        <a href="#" id="add_active_date_range" class="add_link"><img src="<?php echo CMPOPFLY_PLUGIN_URL . 'shared/assets/images/plus.png' ?>" /></a>
        <br />
        <span class='field-info'>You can choose the activity dates for current campaign.</span>
    </p>
    <label>Show on every page</label>
    <p>
        <?php $mb->the_field('cm-campaign-show-allpages'); ?>
        <input type="hidden" name="<?php $mb->the_name(); ?>" value="0"/>
        <input type="checkbox" name="<?php $mb->the_name(); ?>" value="1" <?php checked('1', $metabox->get_the_value()); ?> class="<?php $mb->the_name(); ?>"/>
        <span class='field-info'>If this checkbox is selected then this Campaign will be displayed on each post and page of your website</span>
    </p>

    <label>Show on URLs matching pattern</label>
    <p>
        <?php $mb->the_field('cm-help-item-show-wildcard'); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" placeholder="/help/" value="<?php echo $metabox->get_the_value(); ?>"/>
        <span class='field-info'>If this field is filled widget will be displayed on pages with matching url. Permalinks must be enabled for this function to work.</span>
    </p>

    <label>Show on selected posts/pages</label>
    <?php while ($mb->have_fields_and_multi('cm-help-item-options')): ?>

        <?php $mb->the_group_open(); ?>

        <div class="group-wrap <?php echo $mb->get_the_value('toggle_state') ? ' closed' : ''; ?>" >

            <?php $mb->the_field('toggle_state'); ?>
            <input type="checkbox" name="<?php $mb->the_name(); ?>" value="1" <?php checked('1', $mb->get_the_value()); ?> class="toggle_state hidden" />

            <div class="group-control dodelete" title="<?php _e('Click to remove "Page"', ''); ?>"></div>
            <div class="group-control toggle" title="<?php _e('Click to toggle', ''); ?>"></div>

            <?php $mb->the_field('title'); ?>
            <?php // need to html_entity_decode() the value b/c WP Alchemy's get_the_value() runs the data through htmlentities() ?>
            <h3 class="handle">Page/Post</h3>

            <div class="group-inside">

                <?php
                $mb->the_field('cm-help-item-url');

                $args['name'] = $mb->get_the_name();
                $args['selected'] = $metabox->get_the_value();
                cminds_dropdown($args);
                ?>

            </div><!-- .group-inside -->

        </div><!-- .group-wrap -->

        <?php $mb->the_group_close(); ?>
    <?php endwhile; ?>

    <span class='field-info'>Choose the pages on which current Campaign should be displayed</span>
    <p><a href="#" class="docopy-cm-help-item-options button"><span class="icon add"></span>Add Page</a></p>

    <p class="meta-save"><button type="submit" class="button-primary" name="save"><?php _e('Update'); ?></button></p>

</div>