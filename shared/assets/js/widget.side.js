(function ($) {

    $('#cmpopfly-widget-container .cmpopfly-widget-nav li.link').click(function () {

        var link = $(this).find('a.title-link');

        if (link.length)
        {
            link[0].click();
            return;
        }
        $(this).addClass('active').siblings().removeClass('active');
        var id = $(this).attr('id');

        $('#cmpopfly-widget-container .cmpopfly-widget-content > ul').removeClass('show');
        $('#cmpopfly-widget-container ul.' + id).addClass('show');
    });

    setTimeout(function () {

        $('#cmpopfly-search').fastLiveFilter('#cmpopfly-widget-nav', {
            timeout: 200
        });

    }, 100);

})(jQuery);